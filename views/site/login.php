<div class="loginForm">
    <div class="panel panel-warning">
        <div class="panel-body">
            <h3 class="supermarket" style="margin: 0px 0px 15px; text-align: center;">กรุณาลงชื่อเพื่อใช้งาน</h3>
            <form class="form-horizontal" method='post' id='login_section'>
                <div class="form-group">
                    <div class="col-xs-10 col-xs-offset-1">
                        <input type="text" name="username" required="" class="form-control" placeholder="ชื่อผู้ใช้งาน">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-10 col-xs-offset-1">
                        <input type="password" name="password" required="" class="form-control" placeholder="รหัสผ่าน">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-10 col-xs-offset-1" style="text-align: right;">
                        <button id='login_btn' class='btn btn-warning' data-loading-text="กำลังลงชื่อเข้าใช้..."><i class='glyphicon glyphicon-log-in'></i>&nbsp;ลงชื่อเข้าใช้</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function () {
//        $('#login_section').ajaxForm({
//            beforeSend: function () {
//                $('#login_btn').button('loading');
//            },
//            complete: function(xhr){
//                console.log(xhr);
//            }
//        });
    });
</script>