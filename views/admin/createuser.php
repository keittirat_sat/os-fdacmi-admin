<div class="container">
    <div class="row" style="background-color: #efefef;">
        <div class="col-xs-3">
            <div class="panel panel-default" style="margin-top: 15px;">
                <div class="panel-body">
                    <ul class="nav nav-pills nav-stacked">
                        <li><a href="<?php echo Yii::$app->urlManager->createUrl("admin/listuser") ?>">List of User</a></li>
                        <?php if (Yii::$app->controller->session["auth"]["level"] == 1): ?>
                            <li><a href="<?php echo Yii::$app->urlManager->createUrl("admin/createuser") ?>">Create new User</a></li>
                        <?php endif; ?>
                        <li><a href="<?php echo Yii::$app->urlManager->createUrl(["admin/userinfo", "uid" => Yii::$app->controller->session["auth"]["uid"]]) ?>">My Profile</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-xs-9" style="background-color: #fff;">
            <h3>Application form</h3>
            <form class="form-horizontal" method="post" action="<?php echo Yii::$app->urlManager->createUrl("api/createuser") ?>" id="regist_form">
                <label>ชื่อ - นามสกุล</label>
                <div class="form-group">
                    <div class="col-xs-12">
                        <input type="text" class="form-control" name="realname" placeholder="ชื่อ สกุล" required="">
                    </div>
                </div>

                <label>Username</label>
                <div class="form-group">
                    <div class="col-xs-12">
                        <input type="text" class="form-control" name="username" placeholder="ชื่อเข้าใช้งาน" required="">
                    </div>
                </div>

                <label>Password</label>
                <div class="form-group">
                    <div class="col-xs-12">
                        <input type="password" class="form-control" name="password" placeholder="รหัสผ่านเข้าใช้งาน" required="">
                    </div>
                </div>

                <label>Email</label>
                <div class="form-group">
                    <div class="col-xs-12">
                        <input type="email" class="form-control" name="email" placeholder="Email" required="">
                    </div>
                </div>

                <label>ระดับผู้ใช้งาน</label>
                <div class="form-group">
                    <div class="col-xs-12">
                        <select name="level" class="form-control">
                            <?php foreach (Yii::$app->params["userlevel"] as $val => $label): ?>
                                <option value="<?php echo $val ?>"><?php echo $label ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>

                <label>ประจำอำเภอ</label>
                <div class="form-group">
                    <div class="col-xs-12">
                        <select name="province" class="form-control">
                            <?php foreach (Yii::$app->params["amphor"] as $val => $label): ?>
                                <option value="<?php echo $val ?>"><?php echo $label ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-primary" id="submit_btn" data-loading-text="Creating..."><i class="glyphicon glyphicon-upload"></i>&nbsp;Create</button>
                        <button type="reset" class="btn btn-danger"><i class="glyphicon glyphicon-refresh"></i>&nbsp;Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script typr="text/javascript">
    $(function() {
        $('#regist_form').ajaxForm({
            beforeSend: function() {
                $('#submit_btn').button('loading');
            },
            complete: function(xhr) {
                var json = $.parseJSON(xhr.responseText);
                if (json.status === "success") {
                    swal({"title": "สำเร็จ", "text": "ระบบได้ทำการสร้างบัญชีผู้ใช้ใหม่เรียบร้อยแล้ว", "type": "success"}, function() {
                        location.href = json.link;
                    });
                } else {
                    swal({"title": "ล้มเหลว", "text": "ระบบไม่สามารถสร้างบัญชี", "type": "error"});
                }
                $('#submit_btn').button('reset');
            }
        });
    });
</script>