<div class="container">
    <h3>ระบบอัพโหลดไฟล์</h3>
    <div class="row" style="background-color: #efefef;">
        <div class="col-xs-9 col-xs-offset-3" style="background-color: #fff;">
            <form class='form-horizontal' enctype="multipart/form-data" id='uploadfile_form' method='post' action='<?php echo Yii::$app->urlManager->createUrl("api/uploadallfile") ?>'>
                <label>หัวข้อ</label>
                <div class='form-group'>
                    <div class='col-xs-12'>
                        <input type='text' class='form-control' required="" name='subject'>
                    </div>
                </div>

                <label>เลือกไฟล์</label>
                <div class='form-group'>
                    <div class='col-xs-12'>
                        <input type='file' name='file_upload_zxc'>
                    </div>
                </div>

                <label>รายละเอียด</label>
                <div class='form-group'>
                    <div class='col-xs-12'>
                        <textarea class='form-control' name='detail' rows="12" required=""></textarea>
                    </div>
                </div>

                <div class='form-group'>
                    <div class='col-xs-12'>
                        <span id='spin'><img src='/wp-admin/images/spinner.gif'></span>
                        <button class='btn btn-primary' id='submit_btn' type='submit'><i class='glyphicon glyphicon-upload' data-loading-text='Uploading...'></i>&nbsp;Upload</button>
                        <button class='btn btn-danger' type='reset'><i class='glyphicon glyphicon-refresh'></i>&nbsp;Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<style>
    #spin{
        display:none;
    }
</style>
<script type='text/javascript'>
    $(function () {
        $('#uploadfile_form').ajaxForm({
            beforeSend: function () {
                $('#submit_btn').button("loading");
                $('#spin').show();
            },
            uploadProgress: function (event, position, total, percentComplete) {
                console.log(percentComplete);
            },
            complete: function (xhr) {
                var json = $.parseJSON(xhr.responseText);
                if (json.status === "success") {
                    swal({title: "สำเร็จ", text: "ระบบได้ทำการอัพโหลดข้อมูลสำเร็จ", type: "success"}, function () {
                        location.href = json.link;
                    });
                } else {
                    swal({title: "ล้มเหลว", text: "ระบบไม่สามารถอัพโหลดข้อมูล", type: "error"});
                }

                $('#submit_btn').button("reset");
            }
        });
    });
</script>