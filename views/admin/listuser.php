<div class="container">
    <div class="row" style="background-color: #efefef;">
        <div class="col-xs-3">
            <div class="panel panel-default" style="margin-top: 15px;">
                <div class="panel-body">
                    <ul class="nav nav-pills nav-stacked">
                        <li><a href="<?php echo Yii::$app->urlManager->createUrl("admin/listuser") ?>">List of User</a></li>
                        <?php if (Yii::$app->controller->session["auth"]["level"] == 1): ?>
                            <li><a href="<?php echo Yii::$app->urlManager->createUrl("admin/createuser") ?>">Create new User</a></li>
                        <?php endif; ?>
                        <li><a href="<?php echo Yii::$app->urlManager->createUrl(["admin/userinfo", "uid" => Yii::$app->controller->session["auth"]["uid"]]) ?>">My Profile</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-xs-9" style="background-color: #fff;">
            <?php if (Yii::$app->controller->session["auth"]["level"] == 1): ?>
                <form class='form-horizontal'>
                    <div class='form-group'>
                        <label class='control-label col-xs-1'>Filter:</label>
                        <div class='col-xs-6'>
                            <select class='form-control' name='province' style='width: 213px !important; display: inline-block;'>
                                <option value="">ทุกอำเภอ</option>
                                <?php foreach (Yii::$app->params["amphor"] as $val => $label): ?>
                                    <option value="<?php echo $val ?>" <?php echo $province == $val ? "selected" : ""; ?>><?php echo $label ?></option>
                                <?php endforeach; ?>
                            </select>
                            <button class='btn btn-default' type="submit" style="display: inline-block;">Submit</button>
                        </div>
                    </div>
                    <input type='hidden' name='r' value='admin/listuser'>
                </form>
            <?php endif; ?>

            <table class="table">
                <thead>
                    <tr>
                        <th width='50px'>#</th>
                        <th width='450px'>ชื่อ</th>
                        <th width='75px'>อำเภอ</th>
                        <th>ดูรายละเอียด</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($user as $each_user): ?>
                        <tr>
                            <td><?php echo $each_user->uid ?></td>
                            <td><?php echo $each_user->realname ?></td>
                            <td><?php echo Yii::$app->params["amphor"][$each_user->province] ?></td>
                            <td><a href='<?php echo Yii::$app->urlManager->createUrl(["admin/userinfo", "uid" => $each_user->uid]) ?>' class='btn btn-default'>View</a></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>