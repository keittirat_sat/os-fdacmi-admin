<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">FDACMI</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="<?php echo Yii::$app->urlManager->createUrl("admin/index") ?>">Welcome</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">User Manager <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="<?php echo Yii::$app->urlManager->createUrl("admin/listuser") ?>">List of User</a></li>
                        <?php if ($auth["level"] == 1): ?>
                            <li><a href="<?php echo Yii::$app->urlManager->createUrl("admin/createuser") ?>">Create new User</a></li>
                        <?php endif; ?>
                        <li class="divider"></li>
                        <li><a href="<?php echo Yii::$app->urlManager->createUrl(["admin/userinfo", "uid" => $auth["uid"]]) ?>">My Profile</a></li>
                    </ul>
                </li>
                <li><a href="<?php echo Yii::$app->urlManager->createUrl("admin/upload") ?>">Upload File</a></li>
                <li><a href="<?php echo Yii::$app->urlManager->baseUrl; ?>/webboard">Webboard</li>
            </ul>
            <p class="navbar-text navbar-right">Signed in as <a href="<?php echo Yii::$app->urlManager->createUrl(["admin/userinfo", "uid" => $auth["uid"]]) ?>" class="navbar-link"><?php echo $auth['realname'] ?></a> <a href="<?php echo Yii::$app->urlManager->createAbsoluteUrl("site/logout") ?>" class="btn btn-xs btn-danger">Log out</a></p>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>