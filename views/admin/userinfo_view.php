<div class="container">
    <div class="row" style="background-color: #efefef;">
        <div class="col-xs-3">
            <div class="panel panel-default" style="margin-top: 15px;">
                <div class="panel-body">
                    <ul class="nav nav-pills nav-stacked">
                        <li><a href="<?php echo Yii::$app->urlManager->createUrl("admin/listuser") ?>">List of User</a></li>
                        <?php if (Yii::$app->controller->session["auth"]["level"] == 1): ?>
                            <li><a href="<?php echo Yii::$app->urlManager->createUrl("admin/createuser") ?>">Create new User</a></li>
                        <?php endif; ?>
                        <li><a href="<?php echo Yii::$app->urlManager->createUrl(["admin/userinfo", "uid" => Yii::$app->controller->session["auth"]["uid"]]) ?>">My Profile</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-xs-9" style="background-color: #fff;">
            <h3>Application form</h3>
            <form class="form-horizontal" method="post" id="regist_form">
                <label>ชื่อ - นามสกุล</label>
                <div class="form-group">
                    <div class="col-xs-12">
                        <input type="text" class="form-control" name="realname" placeholder="ชื่อ สกุล" required="" value='<?php echo $userinfo->realname; ?>' readonly="">
                    </div>
                </div>

                <label>Username</label>
                <div class="form-group">
                    <div class="col-xs-12">
                        <input type="text" class="form-control" name="username" placeholder="ชื่อเข้าใช้งาน" required="" value='<?php echo $userinfo->username; ?>' readonly="">
                    </div>
                </div>

                <label>Email</label>
                <div class="form-group">
                    <div class="col-xs-12">
                        <input type="email" class="form-control" name="email" placeholder="Email" required="" value='<?php echo $userinfo->email; ?>' readonly="">
                    </div>
                </div>

                <label>ระดับผู้ใช้งาน</label>
                <div class="form-group">
                    <div class="col-xs-12">
                        <select name="level" class="form-control" readonly="">
                            <?php foreach (Yii::$app->params["userlevel"] as $val => $label): ?>
                                <option value="<?php echo $val ?>" <?php echo $userinfo->level == $val ? "selected" : ""; ?>><?php echo $label ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>

                <label>ประจำอำเภอ</label>
                <div class="form-group">
                    <div class="col-xs-12">
                        <select name="province" class="form-control" readonly="">
                            <?php foreach (Yii::$app->params["amphor"] as $val => $label): ?>
                                <option value="<?php echo $val ?>" <?php echo $userinfo->province == $val ? "selected" : ""; ?>><?php echo $label ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>