<?php

function getFilename($path) {
    // if there's no '/', we're probably dealing with just a filename
    // so just put an 'a' in front of it
    if (strpos($path, '/') === false) {
        $path_parts = pathinfo('a' . $path);
    } else {
        $path = str_replace('/', '/a', $path);
        $path_parts = pathinfo($path);
    }
    return substr($path_parts["filename"], 1);
}
?>

<div class='container'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='panel panel-info'>
                <div class='panel-heading'>
                    <h4>ไฟล์จาก Admin</h4>
                </div> 
                <div class='panel-body'>
                    <table class='table'>
                        <thead>
                            <tr>
                                <th width='50'>#</th>
                                <th>ชื่อไฟล์</th>
                                <th width='250'>อัพโหลดโดย</th>
                                <th width='100'>กระทำ</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($file_admin as $each_file): ?>
                                <tr>
                                    <td><?php echo $each_file->upload_id ?></td>
                                    <td>
                                        <a href='<?php echo $each_file->upload_file ?>'><?php echo getFilename($each_file->upload_file); ?></a>
                                    </td>
                                    <td>
                                        <?php echo $user[$each_file->uid]["realname"] . " (" . Yii::$app->params["amphor"][$user[$each_file->uid]['province']] . ")" ?> 
                                    </td>
                                    <td>
                                        <a class='btn btn-danger btn-xs' href='<?php echo Yii::$app->urlManager->createUrl(["admin/fileinfo", "upload_id" => $each_file->upload_id]) ?>'>View</a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>   
            </div>

            <div class='panel panel-default'>
                <div class='panel-heading'>
                    <h4>ไฟล์อัพโหลด</h4>
                </div>
                <div class='panel-body'>
                    <table class='table'>
                        <thead>
                            <tr>
                                <th width='50'>#</th>
                                <th>ชื่อไฟล์</th>
                                <th width='250'>อัพโหลดโดย</th>
                                <th width='100'>กระทำ</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($file as $each_file): ?>
                                <tr>
                                    <td><?php echo $each_file->upload_id ?></td>
                                    <td>
                                        <a href='<?php echo $each_file->upload_file ?>'><?php echo getFilename($each_file->upload_file); ?></a>
                                    </td>
                                    <td>
                                        <?php echo $user[$each_file->uid]["realname"] . " (" . Yii::$app->params["amphor"][$user[$each_file->uid]['province']] . ")" ?> 
                                    </td>
                                    <td>
                                        <a class='btn btn-danger btn-xs' href='<?php echo Yii::$app->urlManager->createUrl(["admin/fileinfo", "upload_id" => $each_file->upload_id]) ?>'>View</a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
