<div class='container'>
    <div class='row'>
        <div class='col-xs-12'>
            <h2><?php echo $file->subject; ?></h2>
            <div class='panel panel-default'>
                <div class='panel-body'>
                    <?php echo nl2br($file->detail) ?>
                    <p style='text-align: right;'><i class='glyphicon glyphicon-download'></i>&nbsp;Download : <a href='<?php echo $file->upload_file ?>'><?php echo $file->upload_file ?></a></p>
                </div>
            </div>
            <div style='text-align: right'>
                <p style='margin: 0;'><i class='glyphicon glyphicon-user'></i>&nbsp;<?php echo $user->realname; ?></p>
                <p><?php echo Yii::$app->params['province'][$user->province]; ?></p>
            </div>
        </div>
    </div>