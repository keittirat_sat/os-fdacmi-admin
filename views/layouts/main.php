<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>FDACMI :: <?php echo isset(Yii::$app->controller->title) ? Yii::$app->controller->title : "Please login"; ?></title>
        <!--CSS-->
        <link rel="stylesheet" href="<?php echo Yii::$app->getAssetManager()->baseUrl; ?>/f281dddc/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo Yii::$app->getAssetManager()->baseUrl; ?>/css/admin.css">
        <link rel="stylesheet" href="<?php echo Yii::$app->getAssetManager()->baseUrl; ?>/css/sweet-alert.css">

        <!--JS-->
        <script type="text/javascript" src="<?php echo Yii::$app->getAssetManager()->baseUrl; ?>/js/jquery-1.9.1.min.js"></script>        
        <script src="<?php echo Yii::$app->getAssetManager()->baseUrl; ?>/f281dddc/js/bootstrap.min.js"></script>
        <script src="<?php echo Yii::$app->getAssetManager()->baseUrl; ?>/js/jquery.form.js"></script>
        <script src="<?php echo Yii::$app->getAssetManager()->baseUrl; ?>/js/sweet-alert.min.js"></script>
        <style type="text/css">
            /*Responsive disabled*/
            body{
                min-width: 970px;
            }

            .container {
                width: 970px;
            }

        </style>
    </head>
    <body>
        <?php echo isset(Yii::$app->controller->nav) ? Yii::$app->controller->nav : null; ?>
        <?php echo $content ?>
    </body>
</html>
