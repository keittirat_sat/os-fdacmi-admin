<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ext_upload".
 *
 * @property integer $upload_id
 * @property string $upload_file
 * @property string $subject
 * @property string $detail
 * @property integer $uid
 */
class ExtUpload extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ext_upload';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['upload_file', 'subject', 'detail', 'uid'], 'required'],
            [['upload_file', 'subject', 'detail'], 'string'],
            [['uid'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'upload_id' => 'Upload ID',
            'upload_file' => 'Upload File',
            'subject' => 'Subject',
            'detail' => 'Detail',
            'uid' => 'Uid',
        ];
    }
}
