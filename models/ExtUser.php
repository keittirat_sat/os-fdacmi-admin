<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ext_user".
 *
 * @property integer $uid
 * @property string $username
 * @property string $password
 * @property string $realname
 * @property string $email
 * @property integer $province
 * @property integer $level
 */
class ExtUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ext_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'password', 'realname', 'province'], 'required'],
            [['realname', 'email'], 'string'],
            [['province', 'level'], 'integer'],
            [['username'], 'string', 'max' => 30],
            [['password'], 'string', 'max' => 32]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'uid' => 'Uid',
            'username' => 'Username',
            'password' => 'Password',
            'realname' => 'Realname',
            'email' => 'Email',
            'province' => 'Province',
            'level' => 'Level',
        ];
    }
}
