<?php

namespace app\controllers;

use Yii;
use yii\base\Controller;

class ApiController extends Controller {

    public $session;

    public function __construct($id, $module, $config = array()) {
        parent::__construct($id, $module, $config);
        $this->session = Yii::$app->session;
        $this->session->open();

        if (!$this->session['auth']) {
            $target = Yii::$app->getUrlManager()->createUrl('site/index');
            Yii::$app->response->redirect($target);
        }
    }

    public function actionCreateuser() {
        $data = \Yii::$app->getRequest()->post();
        $store = new \app\models\ExtUser;
        foreach ($data as $index => $value) {
            $store->{$index} = ($index != "password") ? $value : md5($value);
        }
        if ($store->save()) {
            $resp['uid'] = $store->uid;
            $resp['status'] = "success";
            $resp['link'] = \Yii::$app->urlManager->createUrl("admin/listuser");
            // New User
            $username = \Yii::$app->getRequest()->post("username");
            $email = \Yii::$app->getRequest()->post("email");
            $realname = \Yii::$app->getRequest()->post("realname");
            $pre_hash_password = \Yii::$app->getRequest()->post("password");
            $body = "<h3>Welcome to FDACMI system</h3>";
            $body .= "<p>Hello {$realname}, here is your information</p>";
            $body .= "<p><b>Username</b>: {$username}</p>";
            $body .= "<p><b>Password</b>: {$pre_hash_password}</p>";
            $body .= "<p><i>You can login at <a href=" . \Yii::$app->urlManager->createUrl("") . ">FDACMI system</a></i></p>";

            $this->actionMailer($email, "New Registration", $body);
        } else {
            $resp['status'] = "fail";
        }
        echo json_encode($resp);
    }

    public function actionUpdateuser() {
        $data = \Yii::$app->getRequest()->post();
        $store = \app\models\ExtUser::find()->where(["uid" => $data['uid']])->one();
        unset($data["uid"]);
        unset($data["username"]);
        foreach ($data as $index => $value) {
            if ($index != "password") {
                $store->{$index} = $value;
            } elseif ($value != "xxxxx") {
                $store->{$index} = md5($value);
            }
        }

        if ($store->save()) {
            $email = \Yii::$app->getRequest()->post("email");
            $realname = \Yii::$app->getRequest()->post("realname");
            $body = "<h3>Hi, {$realname}</h3>";
            $body .= "<p>Your FDACMI Acc has updated</p>";
            $this->actionMailer($email, "Account updated", $body);
            $resp['status'] = "success";
        } else {
            $resp['status'] = "fail";
        }
        echo json_encode($resp);
    }

    public function actionUploadallfile() {
        $post = \Yii::$app->getRequest()->post();
        $folder = date('U');
//        $fpath = \Yii::$app->basePath;
//        $fpath .= "\\upload\\{$this->session["auth"]["uid"]}";

        $fpath = "./upload";
        if (!file_exists($fpath)) {
            @mkdir($fpath);
        }
        
        $fpath .= "/{$this->session["auth"]["uid"]}";
        if (!file_exists($fpath)) {
            @mkdir($fpath);
        }

        $fpath .= "/{$folder}";
        if (!file_exists($fpath)) {
            @mkdir($fpath);
        }

        $store = new \app\models\ExtUpload;
        foreach ($post as $label => $val) {
            $store->{$label} = $val;
        }
        $store->uid = $this->session['auth']['uid'];

        $upload_file = \yii\web\UploadedFile::getInstanceByName("file_upload_zxc");
        $saveFile = "{$fpath}/{$upload_file->name}";
        $upload_file->saveAs($saveFile);
        $store->upload_file = \Yii::$app->urlManager->hostInfo . \Yii::$app->urlManager->baseUrl . "/upload/{$this->session["auth"]["uid"]}/{$folder}/{$upload_file->name}";
        if ($store->save()) {
            $data["status"] = "success";
            $data["link"] = \Yii::$app->urlManager->createUrl("admin/index");
            $body = "<h3>Hi, {$this->session['auth']['realname']}</h3>";
            $body .= "<p>You have new file upload in list {$store->upload_file}</p>";
            if ($this->session['auth']['level'] == 1) {
                //Multiple Email
                $all_user = \app\models\ExtUser::find()->all();
                $from_mail = array();
                foreach ($all_user as $user) {
                    array_push($from_mail, $user->email);
                }
            } else {
                //Single Mail
                $from_mail = $this->session['auth']["email"];
            }
            $this->actionMailer($from_mail, "New file upload", $body);
        } else {
            $data["status"] = "fail";
        }
        echo json_encode($data);
    }

    public function actionMailer($to, $subject, $body, $return = true) {
        $to = isset($to) ? $to : \Yii::$app->getRequest()->post("to", null);
        $subject = isset($subject) ? $subject : \Yii::$app->getRequest()->post("subject", null);
        $body = isset($body) ? $body : \Yii::$app->getRequest()->post("body", null);
        $resp = \Yii::$app->mailer->compose()->setFrom("no-reply@fdacmi.co.th")->setBcc(\Yii::$app->params['adminEmail'])->setTo($to)->setSubject($subject)->setHtmlBody($body)->send();

        if ($resp) {
            $r = true;
            $data['status'] = "success";
        } else {
            $r = false;
            $data['status'] = "fail";
        }

        if ($return) {
            return $r;
        } else {
            echo json_encode($data);
        }
    }

    public function actionDebug() {
        echo \Yii::$app->urlManager->hostInfo . \Yii::$app->urlManager->baseUrl;
    }

    public function __destruct() {
        $this->session->close();
    }

}
