<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;

class AdminController extends Controller {

    public $session;
    public $nav = null;
    public $title = null;
    public $layout = 'main';

    public function __construct($id, $module, $config = array()) {
        parent::__construct($id, $module, $config);
        $this->session = Yii::$app->session;
        $this->session->open();

        if (!$this->session['auth']) {
            $target = Yii::$app->getUrlManager()->createUrl('site/index');
            Yii::$app->response->redirect($target);
        }

        $data['auth'] = $this->session['auth'];
        $this->nav = $this->renderPartial("nav_menu", $data);
    }

    public function actionIndex() {
        $this->title = "Welcome Page";
        $data['auth'] = $this->session['auth'];
        $user = \app\models\ExtUser::find()->all();
        $data["user"] = array();
        foreach ($user as $each_user) {
            $data["user"][$each_user->uid] = array();
            foreach ($each_user as $label => $val) {
                $data["user"][$each_user->uid][$label] = $val;
            }
        }
        if ($this->session['auth']['level'] == 1) {
            $data['file'] = \app\models\ExtUpload::find()->all();
        } else {
            $data['file'] = \app\models\ExtUpload::find()->where(["uid" => $this->session['auth']['uid']])->orderBy("upload_id")->all();
        }

        $data['file_admin'] = \app\models\ExtUpload::find()->join("INNER JOIN", "ext_user", "ext_user.uid = ext_upload.uid")->where(["ext_user.level" => 1])->orderBy("upload_id")->all();
        return $this->render("welcome", $data);
    }

    public function actionFileinfo() {
        $upload_id = \Yii::$app->getRequest()->get("upload_id", null);
        if ($upload_id) {
            $data['file'] = \app\models\ExtUpload::find()->where(["upload_id" => $upload_id])->one();
            $data["user"] = \app\models\ExtUser::find()->where(["uid" => $data["file"]->uid])->one();
            return $this->render("fileinfo", $data);
        } else {
            $target = Yii::$app->getUrlManager()->createUrl('admin/index');
            Yii::$app->response->redirect($target);
        }
    }

    public function actionListuser() {
        $this->title = "List all user";
        if ($this->session['auth']['level'] == 1) {
            $province = \Yii::$app->getRequest()->get("province", null);
            if ($province == null) {
                $data['user'] = \app\models\ExtUser::find()->orderBy('province')->all();
            } else {
                $data['user'] = \app\models\ExtUser::find()->where(["province" => $province])->orderBy('province')->all();
            }
            $data['province'] = $province;
        } else {
            $data['user'] = \app\models\ExtUser::find()->orderBy('province')->where(["province" => $this->session["auth"]["province"]])->all();
        }
        return $this->render("listuser", $data);
    }

    public function actionCreateuser() {
        $this->title = "Create user";
        return $this->render("createuser");
    }

    public function actionUserinfo() {
        $uid = \Yii::$app->getRequest()->get("uid", null);
        if ($uid != null) {
            $data['userinfo'] = \app\models\ExtUser::find()->where(["uid" => $uid])->one();
            $this->title = $data['userinfo']->realname;
            if (($this->session['auth']['uid'] == $uid) || ($this->session['auth']['level'] == 1)) {
                return $this->render("userinfo_edit", $data);
            } else {
                return $this->render("userinfo_view", $data);
            }
        } else {
            $target = \Yii::$app->urlManager->createUrl("admin/listuser");
            \Yii::$app->response->redirect($target);
        }
    }

    public function actionUpload() {
        $this->title = "Upload File";
        return $this->render("uploadfile");
    }

    public function __destruct() {
        $this->session->close();
    }

}
