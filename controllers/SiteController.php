<?php

namespace app\controllers;

use Yii;
use yii\base\Model;
use yii\base\Module;
use yii\base\Controller;
use yii\web\Session;

class SiteController extends Controller {

    public $nav = null;

    public function __construct($id, $module, $config = array()) {
        parent::__construct($id, $module, $config);
    }

    public function actionIndex() {
        $username = Yii::$app->getRequest()->post("username", null);
        $password = Yii::$app->getRequest()->post("password", null);
        if ($username && $password) {
            $password = md5($password);
            $result = \app\models\ExtUser::findOne(["username" => $username, "password" => $password]);
            $session = Yii::$app->session;
            $session->open();

            $info = array();
            foreach ($result as $index => $value) {
                $info[$index] = $value;
            }

            if ($info) {
                $session['auth'] = $info;
                $session->close();
                $target = Yii::$app->getUrlManager()->createUrl('admin/index');
                Yii::$app->response->redirect($target);
            } else {
                $session->close();
            }
        } else {
            return $this->render("login");
        }
    }

    public function actionLogout() {
        $session = Yii::$app->session;
        $session->open();
        $session['auth'] = null;
        Yii::$app->response->redirect(Yii::$app->urlManager->baseUrl);
        $session->close();
    }

    public function actionDebug() {
        $session = Yii::$app->session;
        $session->open();
        echo "<pre>" . print_r($session['auth'], true) . "</pre>";
        $session->close();
    }

    public function actionError() {
        echo "Error Page";
    }

}
